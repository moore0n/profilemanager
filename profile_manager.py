import sublime
import sublime_plugin
import os
import json

PROFILE_FOLDER = sublime.packages_path() + "/ProfileManager/profiles/"
USER_SETTINGS_PATH = sublime.packages_path() + "/User/"

class LoadProfileCommand(sublime_plugin.ApplicationCommand):
	
	settings = sublime.load_settings("ProfileManager.sublime-settings")
	excludes = settings.get("excludes")	

	def run(self):

		self.files = [f for f in os.listdir(PROFILE_FOLDER) if os.path.isfile(PROFILE_FOLDER + f) and str(f) not in self.excludes]
		sublime.active_window().show_quick_panel(self.files, self.file_selected)

	def file_selected(self, result):

		if (result != -1):

			try:
				raw = open(PROFILE_FOLDER + self.files[result])
			
				try: 
					data = json.load(raw)
					self.load_settings(data)
				
				except ValueError, error:
					sublime.error_message("Error parsing JSON: " + str(error))

				raw.close()

			except IOError, error:
				sublime.error_message("Error opening file: " + str(error))

	def load_settings(self, json):

		for settings_file in json:
			self.settings_key = str(settings_file)
			self.settings = sublime.load_settings(self.settings_key);

			self.apply_settings(json[settings_file])

	def apply_settings(self, settings):
		for setting in settings:
			self.settings.set(setting, settings[setting])

		sublime.save_settings(self.settings_key)

		sublime.message_dialog("Profile Loaded.")

class ExportSettingsCommand(sublime_plugin.ApplicationCommand):

	settings = sublime.load_settings("ProfileManager.sublime-settings")

	def run(self):
		sublime.active_window().show_input_panel("Profile Name",  "", self.create_new_profile, None, None)

	def create_new_profile(self, profile_name):
		if not profile_name:
			sublime.error_message("No profile name provided.")
			return

		if os.path.isfile(PROFILE_FOLDER + profile_name + ".profile"):
			if  not sublime.ok_cancel_dialog("Profile already exists, Would you like to overwrite it?"):
				return

		self.export_to_file(profile_name + ".profile")

	def export_to_file(self, filename):

		settings_files = [f for f in os.listdir(USER_SETTINGS_PATH) if os.path.isfile(USER_SETTINGS_PATH + f)]
		
		excludes = self.settings.get("excludes")	
		includes = []

		for settings_file in settings_files:

			if str(settings_file) not in excludes:
				includes.append(settings_file)

		output = "{";

		first = True

		for settings_group in includes:
			
			if first:

				first = False
			else:
				output += ", "

			output += "\"" + str(settings_group) + "\": "

			raw = open(USER_SETTINGS_PATH + settings_group)
			
			output += raw.read()

			raw.close()

		output += "}";

		file_output = open(PROFILE_FOLDER + filename, 'w')
		file_output.write(output)
		file_output.close()

		sublime.message_dialog(filename + " saved.")