{"Preferences.sublime-settings": {
	"auto_match_enabled": false,
	"binary_file_patterns":
	[
		"*.jpg",
		"*.jpeg",
		"*.png",
		"*.gif",
		"*.ttf",
		"*.tga",
		"*.dds",
		"*.ico",
		"*.eot",
		"*.pdf",
		"*.swf",
		"*.jar",
		"*.zip",
		"target/*",
		"min/*"
	],
	"copy_with_empty_selection": false,
	"default_line_ending": "unix",
	"detect_slow_plugins": false,
	"font_options":
	[
		"no_italic"
	],
	"font_size": 12.0,
	"highlight_modified_tabs": true,
	"ignored_packages":
	[
		"Vintage"
	],
	"preview_on_click": false,
	"scroll_past_end": false,
	"spell_check": true,
	"trim_automatic_white_space": true,
	"word_wrap": false
}
}